function MainContainer(parent) {
  this.parent = parent;
  this.bars = [];
  this.currentBar = null;
}

MainContainer.prototype.render = function() {
  var api = new BarAPI();
  var self = this;
  api.getBars(function(err, data) {
    if (err) throw err;

    if (data.buttons && data.bars && data.limit) {
      createBars(data.bars, data.limit);
      createBarsSelector(data.bars);
      createButtons(data.buttons);
    } else {
      throw new Error('Invalid data.');
    }
  });

  function createBarsSelector(bars) {
    var selector = document.createElement('select');
    bars.forEach(function(bar, i) {
      var opt = document.createElement('option');
      opt.value = i;
      opt.innerHTML = ['Bar#', i + 1].join('');
      selector.appendChild(opt);
    });
    selector.onchange = function(e) {
      self.currentBar = self.bars[parseInt(selector.value)];
    };
    self.parent.appendChild(selector);
  }

  function createButtons(buttons) {
    buttons.forEach(function(btn, i) {
      var button = document.createElement('button');
      button.innerHTML = btn;
      button.className = 'button';
      button.onclick = function(e) {
        var value = parseInt(e.target.innerHTML);
        self.currentBar.changeValue(value);
      };
      self.parent.appendChild(button);
    });
  }

  function createBars(bars, limit) {
    bars.forEach(function(bar, i) {
      var barsContainer = document.createElement('div');
      barsContainer.className = 'bar';
      self.parent.appendChild(barsContainer);
      var barDom = new Bar(bar, limit, i, barsContainer);
      self.bars[i] = barDom;
      if (i === 0) {
        self.currentBar = barDom;
      }
    });
  }

};