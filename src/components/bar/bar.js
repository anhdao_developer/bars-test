function Bar(currentValue, maxValue, id, parent) {
  this.currentValue = currentValue;
  this.id = id;
  this.maxValue = maxValue;
  this.label = document.createElement('p');
  this.label.className = 'bar-label';
  this.domElement = document.createElement('div');  
  this.domElement.className = 'bar-fill';
  parent.appendChild(this.domElement);
  parent.appendChild(this.label);
  this.renderPercentageText();
  this.domElement.style.width = [this.getFullPercentage(), '%'].join('');
  this.render();
}

Bar.prototype.renderPercentageText = function() {
  var percentage = this.getFullPercentage();
  if (percentage > 100) {
    this.domElement.className = 'bar-fill bar-fill-overflow';
  } else {
    this.domElement.className = 'bar-fill';
  }
  this.label.innerHTML = [percentage, '%'].join('');
};

Bar.prototype.getFullPercentage = function() {
  return parseInt(this.getPercentage().toFixed(2) * 100);
};

Bar.prototype.getPercentage = function() {
  var percentage = this.currentValue / this.maxValue;
  return percentage;
};

Bar.prototype.render = function() {
  var self = this;
  // Attach the bar to the element
  if (self.domElement.parentNode) {
    self.renderPercentageText();
    animateTo();
  }
  // set base class
  return self.domElement;

  function getDomPercentage() {
    return parseInt(100 * (self.domElement.offsetWidth / self.domElement.parentNode.offsetWidth));
  }

  function animateTo() {
    var widthPercentage = getDomPercentage();
    self.domElement.style.width = [widthPercentage, '%'].join('');
    var newWidthPercentage = self.getFullPercentage();
    var offset = newWidthPercentage - widthPercentage;
    self.step = offset > 0 ? 1 : (offset < 0 ? -1 : 0);
    if (self.step !== 0) {
      if (self.rendering) return;

      changeWidth();
    }

    function changeWidth() {
      widthPercentage += self.step;
      self.domElement.style.width = [widthPercentage, '%'].join('');
      var widthToAnimateToPercentage = self.getFullPercentage();
      if (widthPercentage === widthToAnimateToPercentage || self.domElement.offsetWidth <= 0 || 
          widthPercentage >= 100) {
        cancelAnimationFrame(self.rendering);
        self.rendering = null;
      } else {
        requestAnimationFrame(changeWidth);
      }
    }
  }
};

Bar.prototype.changeValue = function(value) {
  if (this.currentValue + value < 0) {
    if (this.currentValue === 0) return;
    this.currentValue = 0;
  } else {
    this.currentValue += value;
  }
  this.render();
};