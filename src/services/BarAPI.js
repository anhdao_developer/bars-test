
function BarAPI() {

}

BarAPI.prototype.getBars = function(callback) {

  var xhr = new XMLHttpRequest();
  xhr.open('GET', 'http://pb-api.herokuapp.com/bars');
  xhr.onreadystatechange = onResponse;
  xhr.send(null);

  function onResponse() {
    var DONE = 4; // readyState 4 means the request is done.
    var OK = 200; // status 200 is a successful return.
    if (xhr.readyState === DONE) {
      if (xhr.status === OK) {
        try {
          return callback(null, JSON.parse(xhr.responseText));
        } catch(e) {
          return callback(e, null);
        }
      }
    } else {
      // still setting up
    }
  }
};