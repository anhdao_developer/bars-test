describe("Bar changes its state appropriately.", function() {
  var bar;
  var LIMIT = 120;
  var INITIAL_VALUE = 40;
  
  beforeEach(function() {
    // Hidden element
    var div = document.createElement('div');
    bar = new Bar(INITIAL_VALUE, LIMIT, 1, div);
  });

  it("changes the bar's value from 40 to 60", function() {
    bar.changeValue(20);
    expect(bar.currentValue).toEqual(60);
  });


  it("changes the bar's value from 40 to 20", function() {
    bar.changeValue(-20);
    expect(bar.currentValue).toEqual(20);
  });

  it("does not change the bar's value below 0", function() {
    bar.changeValue(-13);
    expect(bar.currentValue).toEqual(27);
    bar.changeValue(-13);
    expect(bar.currentValue).toEqual(14);
    bar.changeValue(-13);
    expect(bar.currentValue).toEqual(1);
    bar.changeValue(-13);
    expect(bar.currentValue).toEqual(0);
    bar.changeValue(-13);
    expect(bar.currentValue).toEqual(0);
  });

  it("can change the bar's value above limit", function() {
    bar.changeValue(13);
    expect(bar.currentValue).toEqual(53);
    expect(bar.getFullPercentage()).toEqual(44);
    bar.changeValue(13);
    expect(bar.currentValue).toEqual(66);
    expect(bar.getFullPercentage()).toEqual(55);
    bar.changeValue(13);
    expect(bar.currentValue).toEqual(79);
    expect(bar.getFullPercentage()).toEqual(66);
    bar.changeValue(30);
    expect(bar.currentValue).toEqual(109);
    expect(bar.getFullPercentage()).toEqual(91);
    bar.changeValue(30);
    expect(bar.currentValue).toEqual(139);
    expect(bar.getFullPercentage()).toEqual(115);
    bar.changeValue(30);
    expect(bar.currentValue).toEqual(169);
    expect(bar.getFullPercentage()).toEqual(141);
  });

});