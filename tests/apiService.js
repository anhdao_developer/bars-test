describe("Bar changes its state appropriately.", function() {
  var apiService = null;
  beforeEach(function() {
    apiService = new BarAPI();
  });

  it('calls the API and returns a response with data we want', function(done) {
    apiService.getBars(function(err, data) {
      expect(err).toBeNull();

      expect(data.bars.length).toBeGreaterThan(0);
      expect(data.limit).toBeGreaterThan(0);
      expect(data.buttons.length).toBeGreaterThan(0);
      done();
    }); 
  });
});