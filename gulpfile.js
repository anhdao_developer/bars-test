/**
 * 
 */
var gulp = require('gulp'),
    concat = require('gulp-concat'),
    minimyCss = require('gulp-minify-css'),
    del = require('del'),
    inject = require('gulp-inject'),
    uglify = require('gulp-uglify'),
    sass = require('gulp-sass');


gulp.task('compile-sass', function() {
  return gulp.src(['./src/components/**/*.scss', './src/mobileStyle.scss', './src/style.scss'])
    .pipe(concat('style.scss'))
    .pipe(sass())
    .pipe(minimyCss())
    .pipe(gulp.dest('./build/'));
});


gulp.task('move-html', function() {
  return gulp.src(['src/index.html'])
      .pipe(gulp.dest('./build'));
});


gulp.task('compile-js', function() {
  return gulp.src(['src/components/**/*.js','src/services/*.js'])
      .pipe(uglify())
      .pipe(gulp.dest('./build'));
});

gulp.task('inject-dependencies', ['compile-sass', 'move-html', 'compile-js'], function() {
  var sources = gulp.src(['./build/**/*.js', './build/*.css'], {read: false});

  return gulp.src('./build/index.html')
    .pipe(inject(sources, { addPrefix: '.', relative:true}))
    .pipe(gulp.dest('./build'));
});

gulp.task('bundle-build', ['inject-dependencies'], function() {

});

gulp.task('clean', function() {
    del(['build/*']);
});

gulp.task('default', ['bundle-build'], function() {
});
