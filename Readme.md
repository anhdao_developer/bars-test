To run the project:

1. npm install
2. bower install
3. gulp clean & gulp to create a build folder with the files
4. You can run SpecRunner.html only after you have built the source code.


******
Animation is done with requestAnimationFrame.

Alternative ways are:
1) With css
2) With setTimeout/setInterval (not very efficient for drawing)

******
API Service is not mocked. It does real calls, so it is not really a unit test.
I wrote it this way because it was easier and it does the job - it tests that we are getting the data we need from the api.